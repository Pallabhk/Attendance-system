<?php
    $filepath=realpath(dirname(__FILE__));
    include_once($filepath.'/database.php')
?>

<?php

class Student{
    private $db;
 public  function __construct(){
    $this->db =new Database();
    }

    public  function  getStudents(){
    $query='SELECT * FROM tbl_student';
        $result=$this->db->select($query);
        return $result;
}
    public  function insertStudents($name,$roll){
        $name =mysqli_real_escape_string($this->db->link,$name);
        $roll =mysqli_real_escape_string($this->db->link,$roll);
        if(empty($name)|| empty($roll)) {
            $mgs="<div class='alert alert-danger'><Strong>Error !</Strong>Field must not be Empty!</div>";
            return $mgs;
        }else{
          $stu_query="INSERT INTO tbl_student(name,roll ) VALUES ('$name','$roll')";
          $stu_insert=$this->db->insert($stu_query);

          $attn_query="INSERT INTO tbl_attendance(roll ) VALUES ('$roll')";
          $stu_insert=$this->db->insert($attn_query);
            if($stu_insert){
                $mgs="<div class='alert alert-success'><Strong>Success !</Strong>Student data insert successfully</div>";
                return $mgs;
            }else{
                $mgs="<div class='alert alert-danger'><Strong>Unsuccessfull !</Strong>Student data does not inserted!</div>";
                return $mgs;
            }
        }
    }
    public function  insertAttendance($cur_date, $attend = array()){
       $query="SELECT DISTINCT atten_time FORM tbl_attendance";
        $getdata=$this->db->select($query);
        while($result=$getdata->fetch_assoc()){
            $db_data =$result['atten_time'];
            if($cur_date=$db_data){
                $mgs="<div class='alert alert-success'><Strong> !</Strong>Attendance All ready taken!</div>";
                return $mgs;
            }
        }
        foreach($attend as $atn_key=>$atn_value){
            if($atn_value == "present"){
             $stu_query="INSERT INTO tbl_attendance(roll,attend,atten_time) VALUES ('$atn_key','present',now())";
           $data_insert =$this->db->insert($stu_query);
            }elseif($atn_value=="absent"){
                $stu_query="INSERT INTO tbl_attendance(roll,attend,atten_time) VALUES ('$atn_key','absent',now())";
                $data_insert =$this->db->insert($stu_query);
            }
        }
        if($data_insert){
            $mgs="<div class='alert alert-success'><Strong>Success !</Strong>Attendance taken Successfully!</div>";
            return $mgs;
        }else{
            $mgs="<div class='alert alert-danger'><Strong>Fail !</Strong>Attendance does not take!</div>";
            return $mgs;
        }
    }
}
?>